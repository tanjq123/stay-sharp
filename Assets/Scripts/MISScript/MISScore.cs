using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MISScore : MonoBehaviour
{
    private Button firstbutton;
    private Button secondbutton;
    private GameObject a5;
    private GameObject a6;
    private GameObject scoreview;
    private GameObject riskview;
    private int score;

    protected static string[] Stringsgetter;

    private bool firstfound;
    private bool secondfound;
    private bool thirdfound;
    private bool fourthfound;
    // Start is called before the first frame update
    void Start()
    {
        firstfound = false; secondfound = false; thirdfound = false; fourthfound = false;
        GameObject ans = GameObject.Find("AnsArea");
        a5 = ans.transform.GetChild(5).gameObject;
        a6 = ans.transform.GetChild(6).gameObject;
        scoreview = ans.transform.GetChild(7).GetChild(1).gameObject;
        riskview = ans.transform.GetChild(7).GetChild(2).gameObject;
        score = 0;
        firstbutton = a5.transform.GetChild(1).GetComponent<Button>();
        secondbutton = a6.transform.GetChild(4).GetComponent<Button>();
        a5.SetActive(false);
        a6.SetActive(false);
        ans.transform.GetChild(7).gameObject.SetActive(false);
        firstbutton.onClick.AddListener(Scoretwo);
        secondbutton.onClick.AddListener(Scoreone);
        secondbutton.onClick.AddListener(settextScore);
    }
    /// <summary>
    ///  <c>Scoretwo</c> Scores for the first memory test check.
    /// </summary>
    void Scoretwo()
    {
        string inputtext = a5.transform.GetChild(2).GetComponent<TMP_InputField>().text;
        string[] arr = inputtext.Split(',');
        foreach(string i in arr)
        {
            
            if(i.Trim().ToUpper() == Stringsgetter[0] && !firstfound)
            {
                score += 2;
                firstfound = true;
            }
            else if (i.Trim().ToUpper() == Stringsgetter[1] && !secondfound)
            {
                score += 2;
                secondfound = true;
            }
            else if (i.Trim().ToUpper() == Stringsgetter[2] && !thirdfound)
            {
                score += 2;
                thirdfound = true;
            }
            else if (i.Trim().ToUpper() == Stringsgetter[3] && !fourthfound)
            {
                score += 2;
                fourthfound = true;
            }
            else { }
        }
    }
    /// <summary>
    /// Scores for the second memory check test.
    /// </summary>
    void Scoreone()
    {
        if(a6.transform.GetChild(0).GetChild(1).GetComponent<TMP_InputField>().text.ToUpper() == Stringsgetter[0])
        {
            if (!firstfound)
            {
                score++;
            }
        }
        if (a6.transform.GetChild(1).GetChild(1).GetComponent<TMP_InputField>().text.ToUpper() == Stringsgetter[1])
        {
            if (!secondfound)
            {
                score++;
            }
        }
        if (a6.transform.GetChild(2).GetChild(1).GetComponent<TMP_InputField>().text.ToUpper() == Stringsgetter[2])
        {
            if (!thirdfound)
            {
                score++;
            }
        }
        if (a6.transform.GetChild(3).GetChild(1).GetComponent<TMP_InputField>().text.ToUpper() == Stringsgetter[3])
        {
            if (!fourthfound)
            {
                score++;
            }
        }
    }

    void settextScore() {
        scoreview.GetComponent<TextMeshProUGUI>().text = "Score: " + score;
        ScoreRecord.MemoryRecord(score);
        TextMeshProUGUI risktext = riskview.GetComponent<TextMeshProUGUI>();
        if (score < 5)
        {
            risktext.text = "Possible cognitive impairment. Advised to check for dementia.";
            risktext.color = new Color(200f, 0f, 0f, 1f);
        }
        else
        {
            risktext.text = "No cognitive impairment detected.";
            risktext.color = new Color(0f, 200f, 0f, 1f);
        }
    }

    public static void setStrings(string[] controlstrings)
    {
        Stringsgetter = controlstrings;
    }

    public string[] getStrings() { return Stringsgetter; }


}
