using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MISControlScript : MonoBehaviour
{
    protected string[,] wordlists = new string[4, 4] {
    {"CHECKERS", "SAUCER", "TELEGRAM", "RED CROSS" },
    {"PEAR", "HAMMER", "COD", "TULIP" },
    {"CHAIR", "FACTORY", "POLICEMAN", "VIOLIN" },
    {"CAR", "NECK", "RABBIT", "BROWN" }};
    protected string[,] typelists = new string[4, 4] {
    {"game", "dish", "message", "company" },
    {"fruit", "tool", "fish", "plant" },
    {"furniture", "building", "person", "instrument" },
    {"vehicle", "body part", "animal", "colour" }};
    protected List<string> wordlist = new List<string>();
    protected List<string> typelist = new List<string>();
    protected string[] wordarray;
    protected string[] typearray;
    int indexno;
    GameObject a1a;
    GameObject a2;
    GameObject a3;
    GameObject a6;
    
    // Start is called before the first frame update
    void Start()
    {
        indexno = Random.Range(0, 3);
        for (int jj = 0;jj<4; jj++)
        {
            wordlist.Add(wordlists[indexno, jj]);
        }
        wordarray = wordlist.ToArray();
        MISScore.setStrings(wordarray);
        for (int ii = 0; ii < 4; ii++)
        {
            typelist.Add(typelists[indexno, ii]);
        }
        typearray = typelist.ToArray();
        a1a = transform.GetChild(0).gameObject;
        a2 = transform.GetChild(2).gameObject;
        a3 = transform.GetChild(3).gameObject;
        a6 = transform.GetChild(6).gameObject;
        setWords(a1a);
        setWords(a3);
        setType(a2);
        setType(a6);
    }

    protected void setWords(GameObject answercanvas) {
        answercanvas.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text =
            wordarray[0] + "\n" + wordarray[1] + "\n" + wordarray[2] + "\n" + wordarray[3];
    }

    protected void setType(GameObject answerCanvas) {
        for (int zz = 0; zz < 4; zz++)
        {
            answerCanvas.transform.GetChild(zz).GetChild(0).GetComponent<TextMeshProUGUI>().text =
                "Which is the " + typearray[zz] + "?";
        }
    }


}
