using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MemoryScoreGUI : MonoBehaviour
{
    GameObject Table;
    private void Start()
    {
        Table = GameObject.Find("BGTable");

        // Display high scores!
        for (int i = 1; i <= ScoreRecord.EntryCount; ++i)
        {
            var entry = ScoreRecord.GetMemoryEntry(i - 1);
            Table.transform.GetChild(i).GetChild(0).GetComponent<TextMeshProUGUI>().text = entry.score.ToString();
            Table.transform.GetChild(i).GetChild(1).GetComponent<TextMeshProUGUI>().text = entry.datetime.ToString();
        }


    }

}
