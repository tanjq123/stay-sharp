using UnityEngine;

using System.Collections.Generic;
using System;

public static class ScoreRecord
{
    public const int EntryCount = 5;

    public struct TrailScoreEntry
    {
        public DateTime datetime;
        public float score;

        public TrailScoreEntry(DateTime datetime, float score)
        {
            this.datetime = datetime;
            this.score = score;
        }
    }

    public struct DigitScoreEntry
    {
        public DateTime datetime;
        public int score;

        public DigitScoreEntry(DateTime datetime, int score)
        {
            this.datetime = datetime;
            this.score = score;
        }
    }
    public struct MemoryScoreEntry
    {
        public DateTime datetime;
        public int score;
        
        public MemoryScoreEntry(DateTime datetime, int score)
        {
            this.datetime = datetime;
            this.score = score;
        }
    }

    private static List<TrailScoreEntry> t_Entries;

    private static List<TrailScoreEntry> tEntries
    {
        get
        {
            if (t_Entries == null)
            {
                t_Entries = new List<TrailScoreEntry>();
                LoadScores();
            }
            return t_Entries;
        }
    }

    private const string PlayerPrefsBaseKey = "leaderboard";

    private static void SortScores()
    {
        t_Entries.Sort((a, b) => b.datetime.CompareTo(a.datetime));
    }

    private static void LoadScores()
    {
        t_Entries.Clear();

        for (int i = 0; i < EntryCount; ++i)
        {
            TrailScoreEntry entry;
            entry.datetime = DateTime.Parse(PlayerPrefs.GetString(PlayerPrefsBaseKey + "[" + i + "].datetime", "Jan 1, 2009"));
            entry.score = PlayerPrefs.GetFloat(PlayerPrefsBaseKey + "[" + i + "].score", 0);
            t_Entries.Add(entry);
        }

        SortScores();
    }

    private static void SaveScores()
    {
        for (int i = 0; i < EntryCount; ++i)
        {
            var entry = t_Entries[i];
            PlayerPrefs.SetString(PlayerPrefsBaseKey + "[" + i + "].datetime", entry.datetime.ToString());
            PlayerPrefs.SetFloat(PlayerPrefsBaseKey + "[" + i + "].score", entry.score);
        }
    }

    public static TrailScoreEntry GetTrailEntry(int index)
    {
        return tEntries[index];
    }

    public static void TrailRecord(float score)
    {
        DateTime datetime = DateTime.Now;
        tEntries.Add(new TrailScoreEntry(datetime, score));
        SortScores();
        tEntries.RemoveAt(tEntries.Count - 1);
        SaveScores();
    }
    private static List<DigitScoreEntry> d_Entries;

    private static List<DigitScoreEntry> dEntries
    {
        get
        {
            if (d_Entries == null)
            {
                d_Entries = new List<DigitScoreEntry>();
                DigitLoadScores();
            }
            return d_Entries;
        }
    }

    private const string DigitPlayerPrefsBaseKey = "digitleaderboard";

    private static void DigitSortScores()
    {
        d_Entries.Sort((a, b) => b.datetime.CompareTo(a.datetime));
    }

    private static void DigitLoadScores()
    {
        d_Entries.Clear();

        for (int i = 0; i < EntryCount; ++i)
        {
            DigitScoreEntry entry;
            entry.datetime = DateTime.Parse(PlayerPrefs.GetString(DigitPlayerPrefsBaseKey + "[" + i + "].datetime", "Jan 1, 2009"));
            entry.score = PlayerPrefs.GetInt(DigitPlayerPrefsBaseKey + "[" + i + "].score", 0);
            d_Entries.Add(entry);
        }

        DigitSortScores();
    }

    private static void DigitSaveScores()
    {
        for (int i = 0; i < EntryCount; ++i)
        {
            var entry = d_Entries[i];
            PlayerPrefs.SetString(DigitPlayerPrefsBaseKey + "[" + i + "].datetime", entry.datetime.ToString());
            PlayerPrefs.SetInt(DigitPlayerPrefsBaseKey + "[" + i + "].score", entry.score);
        }
    }

    public static DigitScoreEntry GetDigitEntry(int index)
    {
        return dEntries[index];
    }

    public static void DigitRecord(int score)
    {
        DateTime datetime = DateTime.Now;
        dEntries.Add(new DigitScoreEntry(datetime, score));
        DigitSortScores();
        dEntries.RemoveAt(dEntries.Count - 1);
        DigitSaveScores();
    }
    private static List<MemoryScoreEntry> m_Entries;

    private static List<MemoryScoreEntry> mEntries
    {
        get
        {
            if (m_Entries == null)
            {
                m_Entries = new List<MemoryScoreEntry>();
                MemoryLoadScores();
            }
            return m_Entries;
        }
    }

    private const string MemoryPlayerPrefsBaseKey = "memoryleaderboard";

    private static void MemorySortScores()
    {
        m_Entries.Sort((a, b) => b.datetime.CompareTo(a.datetime));
    }

    private static void MemoryLoadScores()
    {
        m_Entries.Clear();

        for (int i = 0; i < EntryCount; ++i)
        {
            MemoryScoreEntry entry;
            entry.datetime = DateTime.Parse(PlayerPrefs.GetString(MemoryPlayerPrefsBaseKey + "[" + i + "].datetime", "Jan 1, 2009"));
            entry.score = PlayerPrefs.GetInt(MemoryPlayerPrefsBaseKey + "[" + i + "].score", 0);
            m_Entries.Add(entry);
        }

        MemorySortScores();
    }

    private static void MemorySaveScores()
    {
        for (int i = 0; i < EntryCount; ++i)
        {
            var entry = m_Entries[i];
            PlayerPrefs.SetString(MemoryPlayerPrefsBaseKey + "[" + i + "].datetime", entry.datetime.ToString());
            PlayerPrefs.SetInt(MemoryPlayerPrefsBaseKey + "[" + i + "].score", entry.score);
        }
    }

    public static MemoryScoreEntry GetMemoryEntry(int index)
    {
        return mEntries[index];
    }

    public static void MemoryRecord(int score)
    {
        DateTime datetime = DateTime.Now;
        mEntries.Add(new MemoryScoreEntry(datetime, score));
        MemorySortScores();
        mEntries.RemoveAt(mEntries.Count - 1);
        MemorySaveScores();
    }
}