using UnityEngine;
using TMPro;
public class DigitScoreGUI : MonoBehaviour
{
    GameObject Table;
    private void Start()
    {
        Table = GameObject.Find("BGTable");

        // Display high scores!
        for (int i = 1; i <= ScoreRecord.EntryCount; ++i)
        {
            var entry = ScoreRecord.GetDigitEntry(i-1);
            Table.transform.GetChild(i).GetChild(0).GetComponent<TextMeshProUGUI>().text = entry.score.ToString();
            Table.transform.GetChild(i).GetChild(1).GetComponent<TextMeshProUGUI>().text = entry.datetime.ToString();
        }

        
    }
}
