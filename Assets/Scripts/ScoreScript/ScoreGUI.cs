using TMPro;
using UnityEngine;

public class ScoreGUI : MonoBehaviour
{

    GameObject Table;
    private void Start()
    {
        Table = GameObject.Find("BGTable");

        // Display high scores!
        for (int i = 1; i <= ScoreRecord.EntryCount; ++i)
        {
            var entry = ScoreRecord.GetTrailEntry(i - 1);
            Table.transform.GetChild(i).GetChild(0).GetComponent<TextMeshProUGUI>().text = string.Format("{0:N2}", entry.score);
            Table.transform.GetChild(i).GetChild(1).GetComponent<TextMeshProUGUI>().text = entry.datetime.ToString();
        }


    }
}
