using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Global Class for Score checking, time calculation, node tracking, and error tracking.
public class TrailTestGameControl : MonoBehaviour
{
    //This is for checking whether the person has passed a valid node. 
    public static int nodespass;
    //This is for checking errors made.
    public static int errorsmade;
    //Time passed
    public static float timepassed;
    void Start()
    {
        errorsmade = 0;
        nodespass = 0;
        timepassed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
            timepassed += Time.deltaTime;
        
            
            if (nodespass == 25)
            {
                GameCleared();
                gameObject.GetComponent<TrailTestGameControl>().enabled = false;
            }
        
    }
    /**
     * For when all nodes are passed.
     */
    void GameCleared() {
        GameObject ScoringUI = transform.GetChild(0).GetChild(0).gameObject ;
        Text risktext = transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<Text>();
        ScoringUI.SetActive(true);
        string showscore = "Time: " + string.Format("{0:N2}", timepassed) +  " " + "seconds";
        Debug.Log(showscore);
        ScoringUI.transform.GetChild(1).GetComponent<Text>().text = showscore;
        ScoreRecord.TrailRecord(timepassed);
        if(timepassed < 40)
        {
            risktext.text = "No cognitive impairment detected.";
            risktext.color = new Color(0f, 200f, 0f, 1f);
        }
        else if (timepassed > 85) {
            risktext.text = "High Likelihood of cognitive impairment. Advised to check for dementia.";
            risktext.color = new Color(200f, 0f, 0f, 1f);
        }
        else
        {
            risktext.text = "May be at risk of cognitive impairment. Try again?";
            risktext.color = Color.yellow;
        }
    }
}
