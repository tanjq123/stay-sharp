using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NodeScript : MonoBehaviour
{
    //Used for Node collision when line passes
    CircleCollider2D circlecollide;
    //Checks the node number. Used in error and validity checking.
    private int nodeno;
    // Start is called before the first frame update
    private TextMeshProUGUI textmes;
    private SpriteRenderer render;
    public Button nodebutton;
    void Start()
    {
        transform.position += new Vector3(Random.Range(-0.25f, 0.25f), Random.Range(-0.25f, 0.25f));
        
        circlecollide = GetComponent<CircleCollider2D>();
        TextMeshProUGUI textMeshProUGUI = transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>();
        textmes = textMeshProUGUI;
        
        render = transform.GetChild(0).GetComponent<SpriteRenderer>();
        nodebutton = transform.GetChild(1).GetChild(1).GetComponent<Button>();
        nodebutton.enabled = false;
    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (nodeno == TrailTestGameControl.nodespass)
        {
            //Light up node, e.g.
            // Change Colour of children node
            TrailTestGameControl.nodespass++;
            render.color = new Color(255f, 255f, 255f, 1f);
            textmes.color = new Color(0f, 0f, 0f, 1f);
            circlecollide.enabled = false;
            
        }
        else {
            TrailTestGameControl.errorsmade++;
            //Retry from previous node
            NodeCounting.SetButtonNode(TrailTestGameControl.nodespass);
        }
    }
    public void setnodeno(int n)
    {

        
        nodeno = n;
            

    }
}
