using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Used to set nodes numbers. Also used for starting.
public class NodeCounting : MonoBehaviour
{
    private static GameObject[] NodeList;
    private static MouseLineControl mousecontrol;
    public static GameObject Instruction;
    // Start is called before the first frame update
    void Start()
    {
        NodeList = new GameObject[25];
        
        mousecontrol = GameObject.Find("MouseController").GetComponent<MouseLineControl>();
        NodeList = GameObject.FindGameObjectsWithTag("Node");
        reshuffle(NodeList);
        NodeScript[] scriptlist = new NodeScript[25];
        for (int a = 0; a < (NodeList.Length); a++)
        {
            scriptlist[a] = NodeList[a].GetComponent<NodeScript>();
        }
        //This should set the node order.
        for (int a = 0; a < (NodeList.Length); a++)
        {
            scriptlist[a].setnodeno(a);
            NodeList[a].transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = (a + 1).ToString();
        }
        Instruction = GameObject.Find("InstructionText");
        Instruction.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public static void SetButtonNode(int nodespassed) {
        if (nodespassed > 0)
        {
            NodeList[nodespassed - 1].GetComponent<NodeScript>().nodebutton.enabled = true;
        }
        else
        {
            NodeList[0].GetComponent<NodeScript>().nodebutton.enabled = true;
        }
        mousecontrol.enabled = false;
        Instruction.SetActive(true);
        if (nodespassed > 0)
        {
            Instruction.GetComponent<Text>().text = "Restart from Node " + TrailTestGameControl.nodespass.ToString();
        }
        else
        {
            Instruction.GetComponent<Text>().text = "Restart from Node 1";
        }
    }
    IEnumerator WaitCoroutine()
    {
        

        //yield on a new YieldInstruction that waits for 0.5 seconds.
        yield return new WaitForSeconds(0.5f);

        
    }

    void reshuffle(GameObject[] nodes)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < nodes.Length; t++)
        {
            GameObject tmp = nodes[t];
            int r = Random.Range(t, nodes.Length);
            nodes[t] = nodes[r];
            nodes[r] = tmp;
        }
    }
}
