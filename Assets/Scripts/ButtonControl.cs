using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonControl : MonoBehaviour
{
    private MouseLineControl mousecontrol;
    Button thisbutton; 
    // Start is called before the first frame update
    void Start()
    {
        thisbutton = GetComponent<Button>();
        mousecontrol = GameObject.Find("MouseController").GetComponent<MouseLineControl>();
        thisbutton.onClick.AddListener(Clickrestart);
    }

    // Update is called once per frame
    public void Clickrestart()
    {
        mousecontrol.enabled = true;
        GetComponent<Button>().enabled = false;
        NodeCounting.Instruction.SetActive(false);
    }
}
