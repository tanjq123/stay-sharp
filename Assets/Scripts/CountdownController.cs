using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownController : MonoBehaviour
{
    public int countdownTime;
    public Text countdownDisplay;

    private void Start()
    {
        StartCoroutine(CountdownToStart());
    }

    IEnumerator CountdownToStart()
    {
        while(countdownTime > 0)
        {
            countdownDisplay.text = countdownTime.ToString();

            yield return new WaitForSeconds(1f);

            countdownTime--;
        }
        countdownDisplay.text = "Start!";



        //instance of gamecontroller to add here so game will start once countdown ends
     
        yield return new WaitForSeconds(1f);

        countdownDisplay.gameObject.SetActive(false);
        //start timer , will replace once gamecontroller exists
        TimerController.instance.BeginTimer();

    }
}
