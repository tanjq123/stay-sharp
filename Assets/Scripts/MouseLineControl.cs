using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLineControl : MonoBehaviour
{
    // Game objects for Line rendering
    public GameObject lineprefab;
    public GameObject currLine;
    // GameComponent Objects
    public List<Vector2> mousepos;
    public LineRenderer linerender;
    public EdgeCollider2D edgecollide;
    void Start()
    {

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            createline();
        }
        else if (Input.GetMouseButton(0))
        {
            Vector2 tempmousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Vector2.Distance(tempmousePos, mousepos[mousepos.Count - 1])>0.1f) 
            {
                UpdateLine(tempmousePos);
            }
        }
        if (TrailTestGameControl.nodespass == 25)
        {
            transform.gameObject.SetActive(false);
        }
       
    }

    void createline()
    {
        
        currLine = Instantiate(lineprefab, Vector3.zero, Quaternion.identity);
        linerender = currLine.GetComponent<LineRenderer>();
        
        edgecollide = currLine.GetComponent<EdgeCollider2D>();
        
        mousepos.Clear();
        // Double add to end of the list
        mousepos.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        mousepos.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        linerender.SetPosition(0, mousepos[0]);
        linerender.SetPosition(1, mousepos[1]);
        edgecollide.points = new Vector2[] { };
        edgecollide.points[0] = mousepos[0];
        edgecollide.points[1] = mousepos[1];
    }
    void UpdateLine(Vector2 newmousePos)
    {
        mousepos.Add(newmousePos);
        linerender.positionCount++;
        linerender.SetPosition(linerender.positionCount-1,newmousePos);
        edgecollide.points = new Vector2[] { };
        edgecollide.points = mousepos.ToArray();
 
    }
}
