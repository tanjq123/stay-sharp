using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class DigitGameControl : MonoBehaviour
{

    #region #region Fields
    public Text RoundText;
    public Text scoreText;
    public Text roundShown;
    public Text risktext;

    public InputField InputField = null;
    public int random;
    public string input;
    public string randomVal;
    public int round = 1;
    public int maxRounds = 5;
    public int wrong = 0;

    // To track the right rounds using score
    public int score = 0;
    public GameObject overlay;
    public GameObject restrictPanel;
    public GameObject restrictNum;

    public GameObject tick;
    public GameObject cross;


    #endregion Fields;

    #region Methods


    void Start()
    {
        try
        {
            InputField.characterLimit = 5;
            InputField.readOnly = true;
            scoreText.enabled = false;
            risktext.enabled = false;
            RoundText.enabled = true;

            roundShown.text = "Rounds: " + round.ToString() + " / " + maxRounds.ToString();
            StartCoroutine(UpdateRandom());

        }
        catch (System.NullReferenceException ex)
        {
            Debug.Log("Not set");
        }
    }



    //read the numbers clicked by user
    public void ClickNumber(int val)
    {
        Debug.Log($"check val: { val}");
        input += val;
        InputField.text = input.ToString();
        if (input.Length == 5)
        {
            restrictNum.SetActive(true);
        }
      
    }

    private IEnumerator UpdateRandom()
    {

        restrictPanel.SetActive(true);
        yield return new WaitForSeconds(1);
        tick.SetActive(false);
        cross.SetActive(false);
        RoundText.text = "New Round!";
        RoundText.enabled = true;
        yield return new WaitForSeconds(2);
        RoundText.enabled = false;

        yield return new WaitForSeconds(1);


        int i = 0;
        while (i < 5)
        {
            random = Random.Range(0, 10);
            InputField.text = "" + random;
            i++;  
            yield return new WaitForSeconds(2);
            randomVal += random;
            InputField.text = "";
            yield return new WaitForSeconds(0.5f);
        }

        RoundText.text = "Guess Now";
        RoundText.enabled = true;
        yield return new WaitForSeconds(1);
        restrictNum.SetActive(false);
        restrictPanel.SetActive(false);
        RoundText.enabled = false;

    }

    public void ClickCheck()
    {

        if (round <= maxRounds)
        {
            if (input == randomVal)
            {

                tick.SetActive(true);
                Debug.Log("Correct!");
                randomVal = "";
                random = 0;
                input = "";
                InputField.text = input.ToString();
                round += 1;
                score += 1;
       
                if (round > maxRounds)
                {
                    tick.SetActive(true);
                    Debug.Log("Game Finished!");
                    StartCoroutine(ScoreOverlay());


                }
                else
                {
                    roundShown.text = "Rounds: " + round.ToString() + " / " + maxRounds.ToString();
                    StartCoroutine(UpdateRandom());
                }


            }
            else
            {

                cross.SetActive(true);
                Debug.Log("NOT Correct!");
                randomVal = "";
                random = 0;
                input = "";
                InputField.text = input.ToString();
                round += 1;
                wrong += 1;
       
                if (round > maxRounds)
                {
                    cross.SetActive(true);
                    Debug.Log("Game Finished!");
                    StartCoroutine(ScoreOverlay());

                }
                else
                {
                    roundShown.text = "Rounds: " + round.ToString() + " / " + maxRounds.ToString();
                    StartCoroutine(UpdateRandom());
                }


            }


        }
     

    }


    /*  private void ScoreOverlay()
      {

          overlay.SetActive(true);
          StartCoroutine(UpdateScore());

      }*/
    //score section at the end of a game session
    private IEnumerator ScoreOverlay()
    {
        yield return new WaitForSeconds(1);
        restrictNum.SetActive(false);
        cross.SetActive(false);
        tick.SetActive(false);
        overlay.SetActive(true);
        StartCoroutine(UpdateScore());
    }

    //show the total score at the end of a game session
    private IEnumerator UpdateScore()
    {
        scoreText.enabled = true;
        risktext.enabled = true;
        string scoreToShow = "Score: " + getScore().ToString() + " / " + maxRounds.ToString();
        scoreText.text = scoreToShow;
        ScoreRecord.DigitRecord(score);
        yield return null;
        if (getScore() >2)
        {
            risktext.text = "No cognitive impairment detected.";
            risktext.color = new Color(0f, 200f, 0f, 1f);
        }
        
        else
        {
            risktext.text = "Possibility of cognitive impairment. Advised to check for dementia.";
            risktext.color = new Color(200f, 0f, 0f, 1f);
        }
    }

    public int getScore()
    {        
        return score;
    }

  


    #endregion Methods

    #region Events
    #endregion Events
}
